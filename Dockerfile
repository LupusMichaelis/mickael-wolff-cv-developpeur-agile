# syntax=docker/dockerfile:1.5.1
#
FROM lupusmichaelis/alpine-lighttpd-secure:1.1.2

COPY security-header.conf /etc/lighttpd/
COPY . /var/www/localhost/htdocs
