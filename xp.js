assets.xp =
  [
    { 'period': { 'start': '2022/12' }
    , 'starred': true
    , 'company':
      { 'name': 'Inetum — Vattenfall'
      , 'location':
        { 'city': 'Didenheim'
        , 'country': 'France'
        }
      , 'business': 'Énergie'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP'
      , 'Symfony 1 3 6'
      , 'JavaScript'
      , 'EmberJS 2'
      , 'Git'
      , 'Docker'
      , 'Bash'
      , 'SOAP'
      ]
    , 'missions':
      [ 'Nouvelles fonctionnalités'
      , 'Bugfixing'
      , 'Training d\'un junior'
      , 'Automatisation des tests unitaires sur l\'historique'
      , 'Amélioration de la CI/CD'
      ]
    }
  ,
    { 'period': { 'start': '2021/03', 'end': '2022/08' }
    , 'starred': true
    , 'company':
      { 'name': '3MA Group — Caméléon Digital'
      , 'location':
        { 'city': 'Rouffach'
        , 'country': 'France'
        }
      , 'business': 'Communication'
      }
    , 'role': 'Développeur, Opérations'
    , 'skills':
      [ 'PHP'
      , 'Go'
      , 'Elm'
      , 'Git'
      , 'Docker'
      , 'Bash'
      , 'JavaScript'
      , 'WordPress'
      , 'PrestaShop'
      ]
    , 'missions':
      [ 'Dockerization des plateformes clients'
      , 'Amélioration globale des performances'
      , 'Pipelining du déploiement'
      , 'Rationalisation de l\'hébergement'
      , 'Assistance dans l\'apprentissage d\'outils pas l\'équipe en place'
      , 'Création d\'un Saas pour remplacer un plugin WP'
      ]
    }
  ,
    { 'period': { 'start': '2020/08', 'end': '2020/12'}
    , 'starred': true
    , 'company':
      { 'name': 'Proxiad'
      , 'location':
        { 'city': 'Strasbourg'
        , 'country': 'France'
        }
      , 'business': 'ENI'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP'
      , 'SOAP'
      , 'Oracle DB'
      , 'Git'
      , 'Docker'
      , 'Bash'
      , 'JavaScript'
      ]
    , 'missions':
      [ 'automatisation de déploiement (Bash)'
      , 'modernisation de services SOAP (PHP)'
      , 'mise en place de tests unitaires (PHPUnit)'
      , 'dockerization de services SOAP'
      , 'correction d\'une application Kiosk (PHP, JavaScript)'
      ]
    }
  ,
    { 'period': { 'start': '2019/01', 'end': '2020/02'}
    , 'starred': true
    , 'company':
      { 'name': 'Elpev'
      , 'location':
        { 'city': 'Wittelsheim'
        , 'country': 'France'
        }
      , 'business': 'Communication'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'Elm'
      , 'Sass'
      , 'SCSS'
      , 'Bulma'
      , 'Go'
      , 'LUA'
      , 'PHP'
      , 'Symfony'
      , 'Git'
      , 'Docker'
      , 'Selenium'
      ]
    , 'missions':
      [ 'maintenance de « Proximity » (PHP5)'
      , 'mise en place de tests unitaires (PHPUnit)'
      , 'dockerization de « Proximity »'
      , 'intégration graphique sur le front end de « Px² » (Elm, SCSS, Bulma)'
      , 'tests fonctionnels (Selenium)'
      , 'support à l\'intégration de développeurs externe'
      ]
    }
  ,
    { 'period': { 'start': '2018/01', 'end': '2018/05'}
    , 'starred': true
    , 'company':
      { 'name': 'Telmat Industries'
      , 'location':
        { 'city': 'Soultz Haut–Rhin'
        , 'country': 'France'
        }
      , 'business': 'Network appliances'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP5'
      , 'OAuth'
      ]
    , 'missions':
      [ 'intégration générique d\'APIs externes'
      ]
    }
  ,
    { 'period': { 'start': '2015/03', 'end': '2015/06'}
    , 'starred': false
    , 'company':
      { 'name': 'Rapid Ratings'
      , 'location':
        { 'city': 'Dublin'
        , 'country': 'Irlande'
        }
      , 'business': 'Notation et mesure du risque'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP5'
      , 'Python'
      , 'Django'
      , 'PostgreSQL'
      , 'JavaScript'
      , 'jQuery'
      , 'Twitter Bootstrap'
      , 'Salt'
      , 'Vagrant'
      , 'Git'
      , 'GitHub'
      , 'Jira'
      , 'Confluence'
      ]
    , 'missions':
      [ 'maintenance de l\'application historique (PHP)'
      , 'développement de microservices pour remplacer l\'application historique'
      ]
    }
  ,
    { 'period': { 'start': '2013/07', 'end': '2015/02'}
    , 'starred': true
    , 'company':
      { 'name': '3V Transaction Services, Safe Charge'
      , 'location':
        { 'city': 'Dublin'
        , 'country': 'Irlande'
        }
      , 'business': 'Éditeur de systèmes de gestion de cartes cadeau et de cartes de paiement prépayées'
      }
    , 'role': 'Développeur'
    , 'skills':
      ['PHP5'
      , 'Zend Framework 1 & 2'
      , 'Symfony 2'
      , 'Phing'
      , 'BeHat'
      , 'Composer'
      , 'PHPUnit'
      , 'Oauth 1.0a'
      , 'JavaScript'
      , 'jQuery UI'
      , 'jQuery Mobile'
      , 'Dojo'
      , 'Subversion'
      , 'Git'
      , 'Jira'
      ]
    , 'missions':
      [ 'MoneyButton (StarsCard, OrangeCash, DNA Tapaka)'
      , 'développement d\'outils de déploiement en script shell manipulant PHP composer et Subversion'
      , 'développement de points d\'entrée de l\'API du projet StarsCard'
      , 'développement du composant de communication du frontend avec l\'API dans l\'architecture MoneyButton'
      , 'développement de fonctionnalités dans les frontends'
      , 'intégration de designs graphiques'
      , 'O² Money : maintenance et développement (PHP4 nu)'
      , 'VISA showcase : maintenance et développement (Zend 1)'
      , 'portail du service client (Dojo)'
      , 'écriture de tests comportementaux pour tester l\'API (BeHat)'
      , 'amélioration des scripts d\'exportation des données pour le service de renseignement commercial (business intelligence)'
      ]
    }
  ,
    { 'period': { 'start': '2013/05', 'end': '2013/06'}
    , 'starred': false
    , 'company':
      { 'name': 'Zerogrey'
      , 'location':
        { 'city': 'Dublin'
        , 'country': 'Irlande'
        }
      , 'business': 'Éditeur de sites de vente en ligne'
      }
    , 'role': 'Développeur'
    , 'missions':
      [ 'intégration de contenu'
      , 'correction de bugs'
      ]
    }
  ,
    { 'period': { 'start': '2012/10', 'end': '2013/03'}
    , 'starred': true
    , 'company':
      { 'name': 'Dedsert'
      , 'location':
        { 'city': 'Sandyford'
        , 'country': 'Irlande'
        }
      , 'business': 'Éditeur de sites de paris en ligne'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'Python'
      , 'Bottle'
      , 'PHP5'
      , 'CodeIgniter'
      , 'MySQL'
      , 'Git'
      ]
    , 'missions':
      [ 'mise en place du squelette du frontend de Novaball.com pour les intégrateurs'
      , 'développement du backend JSON REST'
      , 'gestion de l\'envoi des SMS'
      , 'gestion des paiements via l\'API Paypoint'
      , 'gestion de la transmission de la liste des parieurs à l\'assureur'
      ]
    }
  ,
    { 'period': { 'start': '2011/09', 'end': '2012/06'}
    , 'starred': true
    , 'company':
      { 'name': 'Ooblada'
      , 'location':
        { 'city': 'Paris'
        , 'country': 'France'
        }
      , 'business': 'Éditeur de jeux pour la plate–forme Facebook'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP5'
      , 'MySQL'
      , 'Memcached'
      , 'Javascript'
      ]
    , 'missions':
      [ 'améliorations du backend (Treasure Madness, SocialPeopleZ)'
      , 'Graph API : adaptation des jeux aux nouvelles politiques des données privées en utilisant les « real time updates »'
      , 'OpenGraph : intégration des OG objects à Treasure Madness (ticker events pour les achievements) et SocialPeopleZ (scoring)'
      , 'refactoring du code historique'
      , 'amélioration de la couverture des tests unitaires (framework maison)'
      ]
    }
  ,
    { 'period': { 'start': '2011/05', 'end': '2011/08'}
    , 'starred': false
    , 'company':
      { 'name': 'Profeleve.com'
      , 'location':
        { 'city': 'Paris'
        , 'country': 'France'
        }
      , 'business': 'Annuaire en ligne de professeurs'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP5'
      , 'MySQL'
      , 'Zend'
      , 'Framework'
      , 'jQuery'
      ]
    , 'missions':
      [ 'mise en place du projet : hébergement, LAMP, dépôt git, Trac'
      , 'prototypage du site web'
      , 'recrutement d\'un intégrateur graphique'
      ]
    }
  ,
    { 'period': { 'start': '2010/04', 'end': '2011/02'}
    , 'starred': false
    , 'company':
      { 'name': 'Jolt Online Gaming'
      , 'location':
        { 'city': 'Dublin'
        , 'country': 'Irlande'
        }
      , 'business': 'Éditeur de jeux web'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'MongoDB'
      , 'Cassandra'
      , 'Memcached'
      , 'MySQL'
      , 'Pylons'
      , 'JQuery'
      ]
    , 'missions':
      [ 'développement d\'un site de prise de paris sportif (Django)'
      , 'création de l\'interface d\'administration de gestion des contextes de jeu dans « Playboy Party Manager »'
      , 'création de l\'interface d\'ajustement du jeu « Championship Manager: Rivals » pour les game designers'
      , 'mesure de la montée en charge des différents jeux pour assurer leur stabilité'
      ]
    }
    ,
    { 'period': { 'start': '2010/02', 'end': '2010/04'}
    , 'starred': true
    , 'company':
      { 'name': 'Global Electronic Business'
      , 'location':
        { 'city': 'Paris'
        , 'country': 'France'
        }
      , 'business': 'Éditeur du site de rencontres AdopteUnMec.com'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP5'
      , 'MySQL'
      , 'JQuery'
      , 'Prototype'
      , 'Memcached'
      , 'Lighttpd'
      ]
    , 'missions':
      [ 'correction de bugs'
      , 'réécriture des formulaire de paiement et d\'inscription'
      , 'refactoring de l\'authentification et de la gestion des utilisateurs'
      , 'integration du système de paiement Paybox'
      ]
    }
  ,
    { 'period': { 'start': '2010/01', 'end': ''}
    , 'starred': false
    , 'company':
      { 'name': 'Equinoxe Media'
      , 'location':
        { 'city': 'Paris'
        , 'country': 'France'
        }
      , 'business': 'Éditeur de sites de divertissement (porno, gossip, rencontre, buzz média)'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP5' 
      , 'MySQL' 
      , 'JQuery' 
      , 'Memcached'
      ]
    , 'missions':
      [ 'intégration de contenu publicitaire sur Abrutis.com'
      , 'développement d\'un service web (JSON) pour une application sur iPhone'
      ]
    }
  ,
    { 'period': { 'start': '2009/03', 'end': '2009/08'}
    , 'starred': false
    , 'company':
      { 'name': 'Nexen Services (Alterway)'
      , 'location':
        { 'city': 'Colombes'
        , 'country': 'France'
        }
      , 'business': ' Services d\'hébergement internet'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'PHP4' 
      , 'MySQL'
      ]
    , 'missions':
      [ 'développement de maintenant PHP4/MySQL4'
      , 'étude de d\'impact et de la migration du SI de PHP4 vers PHP5'
      , 'formateur PHP pour Anaska (Alterway Training) d\'une équipe de 10 développeurs'
      ]
    }
  ,
    { 'period': { 'start': '2006/10', 'end': '2007/04'}
    , 'starred': false
    , 'company':
      { 'name': 'Armadillo'
      , 'location':
        { 'city': 'Vanves'
        , 'country': 'France'
        }
      , 'business': 'Système de gestion de collections numériques'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'SQL' 
      , 'AJAX'
      ]
    , 'missions':
      [ 'adaptation de « Armadillo Média Web » au Web 2.0'
      ]
    }
  ,
    { 'period': { 'start': '2005/06', 'end': '2006/09'}
    , 'starred': false
    , 'company':
      { 'name': 'Amen France'
      , 'location':
        { 'city': 'Levallois–Perret'
        , 'country': 'France'
        }
      , 'business': 'Service d\'hébergement Internet'
      }
    , 'role': 'Technicien de support'
    , 'skills': []
    , 'missions':
      [ 'assistance dans l\'exploitation de serveurs (Microsoft Server et Linux)'
      , 'gestion des abus en liaison avec les administrateurs système (spam, piratage)'
      , 'conseil technique auprès du service commercial'
      , 'documentation pour les utilisateurs et les collaborateurs'
      , 'formation des nouveaux techniciens aux procédures, produits et outils'
      ]
    }
  ,
    { 'period': { 'start': '2005/03', 'end': '2005/06'}
    , 'starred': false
    , 'company':
      { 'name': 'Amen France'
      , 'location':
        { 'city': 'Levallois–Perret'
        , 'country': 'France'
        }
      , 'business': 'Service d\'hébergement Internet'
      }
    , 'role': 'Technico–commercial'
    , 'missions':
      [ 'vente aux professionnels'
      ]
    }
  ,
    { 'period': { 'start': '2004/06', 'end': '2005/01'}
    , 'starred': false
    , 'company':
      { 'name': 'LNA'
      , 'location':
        { 'city': 'Boulogne–Billancourt'
        , 'country': 'France'
        }
      , 'business': 'Vente et location de matériel informatique aux professionnels'
      }
    , 'role': 'Développeur'
    , 'skills':
      [ 'HTML' 
      , 'PHP' 
      , 'Perl' 
      , 'MySQL'
      ]
    , 'missions':
      [ 'développement de maintenance'
      , 'optimisation des performances du moteur de recherche'
      , 'développement d\'une application de gestion des impressions (Delphi, MSSQL)'
      ]
    }
  ,
    { 'period': { 'start': '2003/01', 'end': '2004/08'}
    , 'starred': false
    , 'company':
      { 'name': 'Com&Com'
      , 'location':
        { 'city': 'Le Plessis–Robinson'
        , 'country': 'France'
        }
      , 'business': 'Éditeur de logiciels de gestions pour la presse'
      }
    , 'role': "Formateur\nTechnicien de support"
    , 'skills':
      [
      ]
    , 'missions':
      [ 'support technique par téléphone et courriels'
      , 'formation des utilisateurs'
      ]
    }
  ];
