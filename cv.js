assets.target = { location: {country: 'France'}};

const monthList =
{ '01': 'Janvier'
, '02': 'Février'
, '03': 'Mars'
, '04': 'Avril'
, '05': 'Mai'
, '06': 'Juin'
, '07': 'Juillet'
, '08': 'Août'
, '09': 'Septembre'
, '10': 'Octobre'
, '11': 'Novembre'
, '12': 'Décembre'
};

const formatDate = (literal) =>
{
  if(!literal)
    return '';

  const [year, month] = literal.split('/');

  return `${monthList[month]} ${year}`;
};

assets.xp = assets.xp
  .filter(xp => xp.starred)
  .map(xp =>
    {
      if(xp.company.location.country == assets.target.location.country)
        delete xp.company.location.country;

      return xp;
    })
  .map(xp =>
    {
      xp.period.start = formatDate(xp.period.start);
      xp.period.end = formatDate(xp.period.end);
      return xp;
    })
;

assets.training = assets.training
  .map(training =>
    {
      if(training.organism.location.country == assets.target.location.country)
        delete training.organism.location.country;

      return training;
    });

assets.skill_set = assets.skill_set
  .map(skill =>
    {
      if(assets.skill_kinds.soft === skill.kind)
        return skill;

      skill.content = skill.content
        .filter((one) =>
          one.level > assets.skill_levels.rookie
            || one.level.dev > assets.skill_levels.rookie
      );
      return skill;
    })
  .filter(skill => skill.content && skill.content.length > 0);

document.title = (
  { 'France': `CV de ${assets.id.name}, ${assets.id.role}.`
  , 'Swiss': `${assets.id.name}, ${assets.id.role}`
  , 'Ireland': `${assets.id.name}, ${assets.id.role}`
  , 'Germany': `${assets.id.name}, ${assets.id.role}`
  })[assets.target.location.country];

$(document.head)
  .append($('<meta>', { name: 'description', 'content': 'CV'}))
  .append($('<meta>', { name: 'author', 'content': assets.id.name}))
  ;

$([
  { 'template': '#header-template'
  , 'target': '#header-canvas'
  }
  ,
  { 'template': '#xp-template'
  , 'target': '#xp-canvas'
  }
  ,
  { 'template': '#skill-template'
  , 'target': '#skill-canvas'
  }
  ,
  { 'template': '#training-template'
  , 'target': '#training-canvas'
  }
]).each((index, element) =>
  {
    const template = $(element.template).html();
    Mustache.parse(template);
    const rendered = Mustache.render(template, assets);
    $(element.target).replaceWith(rendered);
  }
);
